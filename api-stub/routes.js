module.exports = function(server) {

  // Create an API namespace, so that the root does not 
  // have to be repeated for each end point.
	server.namespace('/wordplapi', function() {

		// Return fixture data for '/api/posts/:id'
		server.get('/posts/:id', function(req, res) {
			var post = {
					  "post": {
					    "id": 1,
					    "title": "Rails is omakase",
					    "comments": ["1", "2"],
					    "user" : "dhh"
					  },

					  "comments": [{
					    "id": "1",
					    "body": "Rails is unagi"
					  }, {
					    "id": "2",
					    "body": "Omakase O_o"
					  }]
					};

			res.send(post);
		});

		server.get('/games/:id', function(req, res) {
			var game = {
						"game": {
							"id": 1,
							"playerOne": 300,
							"playerTwo": 400,
							"rows": 7,
							"columns": 7,
							"moves": [ 999 ],
							"letters": ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9'],
							"lettersLeft": 79,
							"created": "Sat Feb 15 2014 01:01:40 GMT-0500 (EST)",
							"lastMove": 999,
							"status": "Joe played WS for 4 points."
						},
						"letters": [
							{	"id": 'a1',
								"text": 'A',
								"row": 4,
								"column": 3,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 1,
								"order": 1,
								"onRack": false,
								"played": false,
								"removed": false
							},
							{	"id": 'a2',
								"text": 'B',
								"row": 0,
								"column": 2,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 1,
								"order": 2,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a3',
								"text": 'H',
								"row": 0,
								"column": 3,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 3,
								"order": 3,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a4',
								"text": 'Y',
								"row": 0,
								"column": 4,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 2,
								"order": 4,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a5',
								"text": 'O',
								"row": 0,
								"column": 5,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 1,
								"order": 5,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a6',
								"text": 'S',
								"row": 0,
								"column": 6,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 1,
								"order": 6,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a7',
								"text": 'Z',
								"row": 0,
								"column": 7,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 10,
								"order": 7,
								"onRack": true,
								"played": false,
								"removed": false
							},
							{	"id": 'a8',
								"text": 'W',
								"row": 4,
								"column": 3,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:13:52 GMT-0500 (EST)",
								"points": 3,
								"order": 7,
								"onRack": false,
								"played": true,
								"removed": false
							},
							{	"id": 'a9',
								"text": 'S',
								"row": 4,
								"column": 4,
								"game": 1,
								"user": 300,
								"lastMovedBy": 300,
								"removedBy": '',
								"time": "Sat Feb 15 2014 01:23:52 GMT-0500 (EST)",
								"points": 2,
								"order": 7,
								"onRack": false,
								"played": true,
								"removed": false
							}
						],

						"moves": [
							{
								"id": 999,
								"rows": 7,
								"columns": 7,
								"lettersOnBoard": [ 'a8', 'a9' ],
								"time": "Sat Feb 15 2014 16:31:38 GMT-0500 (EST)",
								"user": 300
							}
						],

						"users": [
							{
								"id": 400,
								"username": 'BillyJones',
								"name": 'Billy Jones',
								"email": 'billy@jones.com',
								"games": 1,
								"moves": []
							}
						]

						
					}

			res.send(game);
		});

		server.get('/users', function(req, res) {
			var users = {
						"users": [{
							"id": 300,
							"username": 'joebar',
							"name": 'joseph',
							"email": 'joebar@joebar.com',
							"games": [ 1 ],
							"moves": [ 999 ]
						},
						{
							"id": 400,
							"username": 'BillyJones',
							"name": 'Billy Jones',
							"email": 'billy@jones.com',
							"games": 1,
							"moves": []
						}]
					}

			res.send(users);
		});	

		server.get('/login/:id', function(req, res) {
			var user = {
						"user": {
							"id": '300',
							"username": 'joebar',
							"name": 'joseph',
							"email": 'joebar@joebar.com',
							"games": [ 1 ],
							"moves": [ 999 ]
						}
					}

			res.send(user);
		});		

		server.put('/letters/:id', function(req, res) {
			req.body.letter.id = req.params['id'];
			res.send(req.body);

		});

	});

};