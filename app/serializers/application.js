var ApplicationSerializer = DS.RESTSerializer.extend({
//	normalize: function(type, hash, property) {
//		hash.id = hash._id;
//		delete hash._id;
//		return this._super(type, hash, property);
//	},
	serializeHasMany: function(record, json, relationship) {

			var attrs, embed, key, relationshipType;
			key = relationship.key;
			relationshipType = DS.RelationshipChange.determineRelationshipType(record.constructor, relationship);

			// added 'manyToOne' support so that hasMany relationships are included in the hash for PUT/PUST requests
			if (relationshipType === 'manyToNone' || relationshipType === 'manyToMany' || relationshipType === 'manyToOne') {
				json[key] = Ember.get(record, key).mapBy('id');
			}
	}	
});

export default ApplicationSerializer;