export default Ember.ObjectController.extend({

	playEnabled: Ember.computed.notEmpty('lettersPending.[]'),
	
	recallEnabled: Ember.computed.notEmpty('lettersPending.[]'),

	controlsEnabled: true

});