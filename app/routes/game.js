export default Ember.Route.extend({
	model: function(params) {
		return this.store.find('game', params.game_id);
	},

	actions: {

		setNotice: function(message) {
			this.controllerFor('game').set('notice', message);
		},


		buildMove: function(newMove) {

			var lastMove = this.modelFor('game').get('lastMove');
			Ember.Logger.log('gameRoute : buildMove : lastMove...');
			Ember.Logger.log(lastMove);
			Ember.Logger.log('gameRoute : buildMove : newMove...');
			Ember.Logger.log(newMove);
		},

		playPass: function() {
			if(!this.get('controlsEnabled')) return;

			Ember.Logger.log('gameRoute : submitMove');
			var gameModel = this.modelFor('game');


		},

		recallShuffle: function() {
			var gameController = this.controllerFor('game'),
				self = this;

			// recall the letters
			if(gameController.get('recallEnabled')) {

				// .slice #LOL
				var lettersPending = gameController.get('lettersPending').slice(0),
					blanksOnRack = gameController.get('lettersAvailable').filterBy('points', 0);

				// remove blank spaces...
				blanksOnRack.forEach(function(letter){
					if(letter.get('points') === 0) return letter.deleteRecord();
				});
				
				// recall pending letters from the board...
				lettersPending.forEach(function(letter){
					return letter.setProperties({ onRack: true, played: false });
				});

			// shuffle the letters on the rack
			} else {

				var lettersAvailable = gameController.get('lettersAvailable').slice(0);

				shuffleArray(lettersAvailable);
			}

			// Fisher-Yates shuffle
			function shuffleArray(list) {
				var j, temp;

				for(var i = list.length - 1; i > -1; i--) {
					j = Math.floor(Math.random() * (i + 1));
					temp = list[i].get('order');
					list[i].set('order', list[j].get('order'));
					list[j].set('order', temp);
				}
				return list;
			}
		},

		rearrangeLetter: function(letter, order) {
			var lettersOnRack = this.controllerFor('game').get('lettersAvailable');

			

			Ember.Logger.log('gameRoute : rearrangeLetter : ');
			Ember.Logger.log(letter);
			Ember.Logger.log(order);
		}

	},

	debounceSave: function(list) {
		var self = this;
		return Ember.RSVP.all(this.controllerFor('game').get('lettersAvailable').invoke('save')).then(function(resolve) {
			self.send('setNotice', "");
			self.controllerFor('game').set('controlsEnabled', true);
		}, function(reject) {
			self.send('setNotice', "There was an error. Please check your connection.");
		});
	}
	// playEnabled = true if there are pending letters with onRack = false
});