export default Ember.Route.extend({
	
	renderTemplate: function(controller, model) {
		var gameController = this.controllerFor('game');
		this.render('game/index', {
			into: 'game',
			controller: gameController
		});
	}
});