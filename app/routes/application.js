export default Ember.Route.extend({
	
	beforeModel: function(transition) {

		var self = this;

		return Ember.$.get(this.get('loginURL')).then(function(data){

			transition.send('pushUser', data);

		}, function(reject){

			Ember.Logger.log(reject);

		});

	},

	loginURL: function() {
		return [ this.store.adapterFor('application').host, this.store.adapterFor('application').namespace, 'login/300'].join('/');		
	}.property(),

	actions: {

		pushUser: function(data) {

			Ember.Logger.log(this.store);
			var user = data.user || { id: 'guest', name: 'guest', username: 'guest_' };

			Ember.Logger.log(user);
			this.store.push('user', user);

			var userModel = this.store.getById('user', 300) || null;

			this.get('currentUser').set('content', userModel);

			Ember.Logger.log(this.get('currentUser'));

		}
	}

});