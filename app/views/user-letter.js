export default Ember.View.extend({
	init: function() {
		this._super();
//		this.set('elementId', 'letter' + this.get('controller.column'));
	},

	classNames: ['letter', 'click'],
	
	classNameBindings: ['letterPending', 'blankSpace'],

	attributeBindings: ['draggable'],
	
	letterNotOnRack: Ember.computed.not('controller.onRack'),

	letterNotPlayed: Ember.computed.not('controller.played'),

	letterPending: Ember.computed.and('letterNotOnRack', 'letterNotPlayed'),

	blankSpace: Ember.computed.empty('controller.text'),

	draggable: function() {
		return this.get('controller.played') ? 'false' : 'true';
	}.property('controller.played'),

	render: function(buffer) {
		var letter = this.get('controller.text'),
			points = this.get('controller.points') || '';
		buffer.push(letter);
		buffer.push('<span class="letter-points">' + points + '</span>');
	},
	
	click: function(e) {
		var data = this.get('controller').get('model');
		Ember.Logger.log(data);

	},

	mouseDown: function(e) {
		var $target = Ember.$(e.target);
		$target.addClass('letter-selected');
	},

	mouseUp: function(e) {
		var $target = Ember.$(e.target);
		$target.removeClass('letter-selected');		
	},

	dragStart: function(e) {
		var $target = Ember.$(e.target),
			data = this.get('controller').getProperties('id', 'text', 'points', 'played');

		// stop blank tiles from being dragged around 
		if(data.text === '' && data.points === 0 || data.played === true) return e.dataTransfer.effectAllowed = 'none';
		data = JSON.stringify(data);
		e.dataTransfer.setData('text/plain', data);

		$target.addClass('letter-selected');		
	},

	dragEnd: function(e) {
		var $target = Ember.$(e.target);
		$target.removeClass('letter-selected');
		this.get('controller').send('setNotice', '');
	},

	dragEnter: function(e) {

		var $target = Ember.$(e.target),
			isAllowed = e.dataTransfer.types.contains('text/plain'),
			letterController = this.get('controller'),
			dragRelationship = letterController.get('onRack') ? 'letter-dragover' : ( letterController.get('text') ? 'swap-dragover' : 'blank-dragover' ),
			message = ('letter-dragover' === dragRelationship) ? '' : ('swap-dragover' === dragRelationship) ? ('Swap letter for ' + letterController.get('text')) + '.' : ('Place letter here.');

		$target.addClass(dragRelationship);
		this.get('controller').send('setNotice', message);

		if(isAllowed) { 
			e.preventDefault();
			return false;
		}
	},

	dragLeave: function(e) {
		var $target = Ember.$(e.target);
		var className = this.get('controller.onRack') ? 'letter-dragover' : ( this.get('controller.text') ? 'swap-dragover' : 'blank-dragover' );
		$target.removeClass(className);

		e.preventDefault();
		return false;
	},

	dragOver: function(e) {
		e.preventDefault();
		return false;
	},

	drop: function(e) {

		var $target = Ember.$(e.target),
			letterController = this.get('controller'),
			destination = letterController.getProperties('row', 'column', 'onRack', 'order', 'played'),

			data = JSON.parse(e.dataTransfer.getData('text/plain')),
			letterId = data.id,

			droppedLetter = letterController.get('store').getById('letter', letterId),
			userModel = droppedLetter.get('user'),
			game = letterController.get('store').getById('game', droppedLetter.get('game.id'));

		// drop is occuring on a blank space
		if(!letterController.get('text')) {

			// the droppedLetter came from the rack...
			// replace its rack space with a blank tile and update the tiles 'row', 'column', and 'onRack' attributes
			if(droppedLetter.get('onRack')) {
								
				// create a blank tile where the droppedLetter came from...
				var blankSpace = letterController.get('store').createRecord('letter', {
																	text: '',
																	row: droppedLetter.get('row'),
																	column: droppedLetter.get('column'),
																	user: droppedLetter.get('user'),
																	time: new Date(),
																	points: 0,
																	order: droppedLetter.get('order'),
																	onRack: true,
																	played: false,
																	removed: false
				});
				game.get('letters').pushObject(blankSpace);
			}

			// updating this view's controller's model with the model from the droppedLetter
			droppedLetter.setProperties({ row: destination.row, 
											column: destination.column, 
											lastMovedBy: userModel, 
											onRack: destination.onRack, 
											played: droppedLetter.get('played'), 
											game: game 
										});

			letterController.send('setNotice', 'Letter ' + droppedLetter.get('text') + ' placed on the board.');

			if(destination.onRack) {
				droppedLetter.set('order', destination.order);
				letterController.set('onRack', false);
				game.get('letters').removeObject(letterController.get('model'));
				letterController.send('setNotice', 'Letter ' + droppedLetter.get('text') + ' placed on rack.');
			}

			droppedLetter.save().then(function(resolve) {

//				letterController.set('model', resolve);

			}, function(reject) {
				droppedLetter.rollback();
				letterController.send('setNotice', "There was an error. Please check your connection.");
			});

		// drop is occuring on an existing letter. we do a swap
		} else {
			$target.removeClass('letter-dragover swap-dragover');		

			// disallow swapping with letters that have been played
			if(!destination.onRack && destination.played) return;

			// moving letters around on the rack
			if(destination.onRack) {
				var initialOrder = droppedLetter.get('order');

				letterController.send('rearrangeLetter', droppedLetter, destination.order);

			}


//			letterController.setProperties({ row: droppedLetter.get('row'), column: droppedLetter.get('column'), order: droppedLetter.get('order')  });

			Ember.Logger.log('swap');

		}
	}

});