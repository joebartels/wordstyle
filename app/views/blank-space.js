export default Ember.View.extend({
	
	init: function() {
		this._super();
//		this.set('elementId', 'letter' + this.get('controller.column'));
	},

	classNames: ['blank-space', 'click'],
	attributeBindings: ['draggable'],
	draggable: 'true',
	
	dragEnter: function(e) {
		var $target = Ember.$(e.target);
		$target.addClass('blank-space-dragover');
	},
	dragLeave: function(e) {
		var $target = Ember.$(e.target);
		$target.removeClass('blank-space-dragover');
	}

});