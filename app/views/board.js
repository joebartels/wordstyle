export default Ember.View.extend({

	templateName: 'board',

	classNames: ['board'],

/*
	numRows: function(){
		var lastMove = this.get('controller.lastMove') || false;
		return lastMove ? lastMove.get('rows') || 7 : 7;
	}.property('controller.lastMove'),

	numColumns: function(){
		var lastMove = this.get('controller.lastMove') || false;
		return lastMove ? lastMove.get('columns') || 7 : 7;
	}.property('controller.lastMove'),
*/
	boardRows: function() {
		var gameController = this.get('controller'),
			numRows = gameController.get('rows') || 7,
			numColumns = gameController.get('columns') || 7,
			user = gameController.get('store').getById('user', this.get('currentUser.id')),
			letters = gameController.get('lettersOnBoard').slice(0),
			rows = [],
			row, column,
			blankSpace = { text: 'blank' };

//		letters = Ember.makeArray(letters.get('content'));

		// iterate through each row of the game board and build the columns one at a time.
		for(var i = 0; i < numRows; i++){
			
			rows[i] = [];

			// iterate through the number of columns.
			for(var j = 0; j < numColumns; j++){
				// if there are no letters than we must simply push a blank letter into the current row's column element.
				if(!letters) {
					rows[i][j] = this.get('controller').get('store').createRecord('letter', {
																	text: '',
																	row: i,
																	column: j,
																	user: user,
																	time: new Date(),
																	points: 0,
																	order: 0,
																	onRack: false,
																	played: true,
																	removed: false
					});
					continue;
				}

				// if there are letters we look at each one and see if it matches the current row/column position.

				for(var k = 0; k < letters.length; k++){

					if( i === letters[k].get('row') && j === letters[k].get('column')) {

						// if this tile is occupied, we check if it has been played.
						// if occupying tile has been played, we put the currently iterated tile back onRack
						if(Ember.isEmpty(rows[i][j])) {

							rows[i][j] = letters[k];

						} else {

							if(rows[i][j].get('played')) {
								letters[k].set('onRack', true);
							} else {
								rows[i][j].set('onRack', true);
								rows[i][j] = letters[k];
							}

							// must break loop here because if we break it earlier, we risk pending letters not being returned to the rack
							break;
						}
					}

				}

				// no letters matched the row/column position, so we make it a blankSpace
				if(!rows[i][j]) rows[i][j] = this.get('controller').get('store').createRecord('letter', {
																	text: '',
																	row: i,
																	column: j,
																	user: user,
																	time: new Date(),
																	points: 0,
																	order: 0,
																	onRack: false,
																	played: true,
																	removed: false
					});

			}
		}
		Ember.Logger.log('boardView : boardRows : rendering board...');
		return rows;

	}.property('controller.letters.@each.row', 'controller.letters.@each.column'),

});