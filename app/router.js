var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {

	this.resource('games', function() {
		this.resource('game', { path: ':game_id' }, function() {
			this.resource('move', { path: ':move_id' });
		});
	});

});

export default Router;
