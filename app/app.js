import Resolver from 'resolver';
import currentUser from 'appkit/objects/current_user';

var App = Ember.Application.extend({
  LOG_ACTIVE_GENERATION: true,
  LOG_MODULE_RESOLVER: true,
  LOG_TRANSITIONS: true,
  LOG_TRANSITIONS_INTERNAL: true,
  LOG_VIEW_LOOKUPS: true,
  modulePrefix: 'appkit', // TODO: loaded via config
  Resolver: Resolver['default']
});



Ember.Application.initializer({
  name: "currentUser",

  initialize: function(container, app) {
    app.register('user:current', currentUser);
    app.inject('controller', 'currentUser', 'user:current');
    app.inject('route', 'currentUser', 'user:current');
    app.inject('socket:main', 'currentUser', 'user:current');
  }
});

export default App;
