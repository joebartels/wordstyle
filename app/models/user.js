export default DS.Model.extend({
	name: DS.attr('string'),
	username: DS.attr('string'),
	email: DS.attr('string'),
	games: DS.hasMany('game', { inverse: 'playerOne' }),
	moves: DS.hasMany('move', { async: true }),
});