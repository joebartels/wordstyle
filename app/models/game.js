export default DS.Model.extend({

	playerOne: DS.belongsTo('user', { inverse: 'games' }),
	playerTwo: DS.belongsTo('user'),

	rows: DS.attr('number'),
	columns: DS.attr('number'),
	moves: DS.hasMany('move'),

	letters: DS.hasMany('letter'),		// letters user has used on the board
	lettersLeft: DS.attr('number'),

	created: DS.attr('date'),
	status: DS.attr('string'),

	lettersAvailable: Ember.computed.filterBy('letters', 'onRack', true),
	lettersAvailableSorted: Ember.computed.sort('lettersAvailable', 'order'),

	lettersOnBoard: Ember.computed.filterBy('letters', 'onRack', false),
	lettersPending: Ember.computed.filterBy('lettersOnBoard', 'played', false),
	sortedMoves: Ember.computed.sort('moves', 'time'),

	order: ['order'],	// required for the 2nd argument of Ember.computed.sort
	time: ['time'],		// required for the 2nd argument of Ember.computed.sort
});