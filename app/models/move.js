export default DS.Model.extend({

	rows: DS.attr('number'),
	columns: DS.attr('number'),
	lettersOnBoard: DS.hasMany('letter'),
	time: DS.attr('date'),
	user: DS.belongsTo('user')
	
});