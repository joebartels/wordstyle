export default DS.Model.extend({

	text: DS.attr('string'),	// letter text
	row: DS.attr('number'),		// row on board letter belongs to. 0 index. top-to-bottom
	column: DS.attr('number'),	// column on board letter belongs to. 0 index. left-to-right
	game: DS.belongsTo('game'),	// game letter belongs to
	user: DS.belongsTo('user'),	// the most recent user to use this letter
	time: DS.attr('date'),		// time letter was generated, moved, played -whichever was most recent
	points: DS.attr('number'),	// # points the letter is worth
	order: DS.attr('number'),	// this is the order of the letter on the user's rack
	onRack: DS.attr('boolean'),	// TRUE if letter is sitting in the user's rack
	played: DS.attr('boolean'),	// TRUE if letter has been submitted
	removed: DS.attr('boolean') // TRUE if letter has been deleted from the board
	
});